package Model;

/**
 * Created by julia on 12/09/2016.
 */
public class ListasPeliculasModel {
    private String nombre;
    private String img;
    private String descropcion;
    private String fecha;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescropcion() {
        return descropcion;
    }

    public void setDescropcion(String descropcion) {
        this.descropcion = descropcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
