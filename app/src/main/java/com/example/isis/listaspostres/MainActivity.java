package com.example.isis.listaspostres;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Model.ListasPeliculasModel;
import adapter.AdapterPostres;

public class MainActivity extends AppCompatActivity {

    private ListView listaPostres;
    private AdapterPostres adapter;
    private List<ListasPeliculasModel> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        data = new ArrayList<ListasPeliculasModel>();
        ListasPeliculasModel pelicula = new ListasPeliculasModel();
        pelicula.setNombre("Terminator Genesis");
        pelicula.setImg("http://peliculasm.tv/archivos/img/3196.jpg");
        pelicula.setDescropcion("Descripcion de terminator genesis");
        pelicula.setFecha("12/09/2015");
        data.add(pelicula);

        ListasPeliculasModel pelicula2 = new ListasPeliculasModel();
        pelicula2.setNombre("Civil war");
        pelicula2.setImg("http://blogs-images.forbes.com/erikkain/files/2016/05/Captain-America-Civil-War-concept-art-1-1200x641.jpg");
        pelicula2.setDescropcion("Descripcion de genesis");
        pelicula2.setFecha("12/04/2014");
        data.add(pelicula2);


        ListasPeliculasModel pelicula3 = new ListasPeliculasModel();
        pelicula3.setNombre("Iro man");
        pelicula3.setImg("http://www.geeksofdoom.com/GoD/img/2013/05/2013-05-03-iron_man.jpg");
        pelicula3.setDescropcion("Descripcion de iro man");
        pelicula3.setFecha("07/09/2014");
        data.add(pelicula3);


        listaPostres = (ListView) findViewById(R.id.lista_postres);
        adapter = new AdapterPostres(this,data);
        listaPostres.setAdapter(adapter);
        listaPostres.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this,"Seleccion: "+data.get(position).getNombre(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}
