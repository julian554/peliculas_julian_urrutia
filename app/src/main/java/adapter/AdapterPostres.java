package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.isis.listaspostres.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import Model.ListasPeliculasModel;

/**
 * Created by julia on 12/09/2016.
 */
public class AdapterPostres extends BaseAdapter {

    Context context;
    List<ListasPeliculasModel> data;
    public AdapterPostres(Context context,List<ListasPeliculasModel> data){
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v==null){
            v = View.inflate(context, R.layout.ly_items_lista_postres,null);
        }
        TextView tv_titulo = (TextView) v.findViewById(R.id.titulo);
        ImageView img = (ImageView) v.findViewById(R.id.img);
        TextView tv_descripcion = (TextView) v.findViewById(R.id.descripcion);
        TextView tv_fecha = (TextView) v.findViewById(R.id.fecha);

        ListasPeliculasModel pelicula = (ListasPeliculasModel) getItem(position);
        tv_titulo.setText(pelicula.getNombre());
        Picasso.with(context).load(pelicula.getImg()).into(img);
        tv_descripcion.setText(pelicula.getDescropcion());
        tv_fecha.setText(pelicula.getFecha());
        if(position % 2 == 0){
            v.setBackgroundColor(v.getResources().getColor(R.color.colorAccent));
        }else{
            v.setBackgroundColor(v.getResources().getColor(R.color.colorPrimary));
        }
        return v;
    }
}
